/* @preserve
 *
 * Calcul d'itinéraire vélo via OpenRoute service
 * https://parcours.scasb.org 
 * 
 * This file is a part of the self hosted webapp mantioned above, 
 * it is free software: you can redistribute it and/or modify it under 
 * the terms of the GNU Affero General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, 
 * or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * Copyright (c) 20Z0 Gérald Niel (https://framagit.org/gegeweb)
 *
 * @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-v3-or-later
 */
(function(global, L) {
	window.addEventListener('load', async() => {
		'use strict';

		L = (typeof window !== "undefined" ? window['L'] : typeof global !== "undefined" ? global['L'] : null);

		// API Key OpenRoute Service
		let headers = new Headers(),
			request = new Request('config/ors_token.txt')
		headers.append('pragma', 'no-cache');
		headers.append('cache-control', 'no-cache');
		const ors_token = await fetch(request, {
				method: 'GET',
				headers: headers
			})
			.then(async response => { return response.text() });

		// global scope
		let controlDownload, controlFit;

		const mylocale = {
			"Total Length: ": "Dist.&nbsp;: ",
			"Max Elevation: ": "Alt. max&nbsp;: ",
			"Min Elevation: ": "Alt. min&nbsp;: ",
			"Total Ascent: ": "D+ ",
			"Total Descent: ": "D- ",
		};

		L.registerLocale('fr', mylocale);
		L.setLocale('fr');

		/*
		 *  ----------------------------------
		 *           Initialize Map
		 *  ----------------------------------
		 */

		const mapBounds = L.latLngBounds(L.latLng(51.0884, 9.5597), L.latLng(41.367, -5.103600));
		const map = L.map('map', {
			zoomControl: false,
		}).fitBounds(mapBounds, {
			padding: [5, 5]
		});
		map.attributionControl.addAttribution(
			'Routes: <a href="https://openrouteservice.org/terms-of-service/">Openroute Service</a> | \
        <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		).setPrefix(
			'<a href="https://leafletjs.com/" title="A JS library for interactive maps">Leaflet</a> | \
        &copy;2020 Gérald Niel - AGPL v3 - sources sur <a href="https://framagit.org/gegeweb/scasb-calcul-itineraires-velo">Framagit</a>'
		);

		// Control Zoom
		L.control.zoom({
			position: 'topright',
			zoomInTitle: 'Zoommer',
			zoomOutTitle: 'Dézoomer'
		}).addTo(map);

		// Control Scale
		L.control.scale({
			imperial: false,
			position: 'bottomright'
		}).addTo(map);

		// center to user position
		map.locate({ setView: true, maxZoom: 15 });

		/*
		 *  ----------------------------------
		 *           Base Layer(s)
		 *  ----------------------------------
		 */

		const baseLayers = {
			'OSM France': L.tileLayer(
				'https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
					attribution: 'Map: <a href="https://www.openstreetmap.fr/mentions-legales/">OpenStreetMap France</a>, \
            Map Data &copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
				}
			).addTo(map),
			'OSM Mapnik': L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
				maxZoom: 19,
				attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
			}),
			'Cyclo OSM': L.tileLayer('https://dev.{s}.tile.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png', {
				maxZoom: 20,
				attribution: '<a href="https://github.com/cyclosm/cyclosm-cartocss-style/releases" title="CyclOSM - Open Bicycle render">CyclOSM</a> \
            | Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
			}),
			'IGN': L.tileLayer(layerIGN(), {
				attribution: 'Map: &copy <a target="_blank" href="https://www.geoportail.gouv.fr/">Geoportail France</a>'
			}),
			'IGN Scan Express': L.tileLayer(layerIGN('GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD'), {
				attribution: 'Map: &copy <a target="_blank" href="https://www.geoportail.gouv.fr/">Geoportail France</a>'
			}),
		};

		const reliefLayer = L.tileLayer('https://tiles.wmflabs.org/hillshading/{z}/{x}/{y}.png', {
			attribution: 'Hillshading: SRTM3 v2 (<a href="http://www.nasa.gov/">NASA</a>)'
		}).addTo(map);
		const overlayLayers = {
			'Relief': reliefLayer
		};

		/*
		 *  ----------------------------------
		 *              Sidebar
		 *  ----------------------------------
		 */

		var sidebar = L.control.sidebar({
			autopan: true,
			closeButton: true,
			container: 'sidebar',
		}).addTo(map);

		// buttons
		sidebar.addPanel({
			id: "locate",
			tab: '<i class="icon icon-locate"></i>',
			title: 'Localiser ma position',
			button: _ => {
				map.locate({ setView: true, maxZoom: 16 });
				/* 
				map.once('locationfound', (e) => {
					addWaypoint(e.latlng);
				});
				*/
			}
		});

		sidebar.addPanel({
			id: "fitroute",
			tab: '<i class="icon icon-trace"></i>',
			title: 'Centrer et zoomer sur la trace',
			button: (e) => {
				let bounds = controlRouting.getLine().getBounds();
				map.fitBounds(bounds, {
					padding: [50, 50]
				});
			}
		});
		sidebar.disablePanel('fitroute');

		sidebar.addPanel({
			id: "dltrace",
			tab: '<i class="icon icon-download"></i>',
			title: 'Télécharger la trace au format GPX',
			button: (e) => {
				let route = controlRouting.getSelectedRoute();
				if (route) {
					let coordinates = route.coordinates,
						polyline = L.polyline(coordinates),
						geojson = polyline.toGeoJSON(),
						date = new Date(),
						isodate = date.toISOString(),
						name = 'Itinéraire - ' + isodate,
						filename = 'trace.' + isodate + '.gpx';

					geojson.properties.name = name;

					let gpx = togpx(geojson, {
						creator: 'togpx orpenrouteservice',
						metadata: {
							name: name,
							desc: 'Trace exoprté depuis SCASB - Calcul Itinéraire',
							author: {
								name: 'Openrouservice',
								email: {
									'@id': 'support',
									'@domain': 'openrouteservice.org'
								},
								link: {
									'@href': 'https://openrouteservice.org/',
									text: 'https://openrouteservice.org/',
									type: 'text/html'
								},
							},
							copyright: {
								'@author': 'openrouteservice.org | OpenStreetMap contributors',
								year: date.getFullYear(),
								licence: 'LGPL 3.0',
							},
							time: date,

						}
					});

					downloadGPX(gpx, filename);
				}
			}
		});
		sidebar.disablePanel('dltrace');

		sidebar.addPanel({
			id: 'display-elevation',
			tab: '<i class="icon icon-elevation"></i>',
			title: 'Afficher/Masquer le profil altimétrique',
			button: (e) => {
				if (elevationControl._container.style.display === "none")
					elevationControl.show();
				else
					elevationControl.hide();
			},
			position: 'bottom'
		});
		sidebar.disablePanel('display-elevation');

		sidebar.addPanel({
			id: 'display-info',
			tab: '<i class="icon icon-info"></i>',
			title: 'Informations',
			button: (e) => {
				let div = document.getElementsByClassName('modal')[0],
					close = document.getElementsByClassName("close")[0];
				div.style.display = "block";

				close.onclick = function() {
					div.style.display = "none";
				}
				window.onclick = function(event) {
					if (event.target == div) {
						div.style.display = "none";
					}
				}
			},
			position: 'bottom'
		});

		/*
		 *  ----------------------------------
		 *             Controls
		 *  ----------------------------------
		 */

		// Layers Control
		const controlLayer = L.control.layers(baseLayers, overlayLayers, {
			collapsed: false,
		});
		controlLayer.addTo(map);
		setParent(controlLayer.getContainer(), L.DomUtil.get('layers-pane'));

		// Geocoder to use with Geocoder and Rounting controls
		const orsGeocoder = L.Control.Geocoder.openrouteservice(ors_token, {
			geocodingQueryParams: {
				'boundary.country': 'FR'
			},
			reverseQueryParams: {
				'boundary.country': 'FR',
				size: 1
			},
			language: 'fr',
		});

		// Control Geocoder
		const controlGeocoder = L.Control.geocoder({
				position: 'topright',
				defaultMarkGeocode: false,
				showResultIcons: true,
				geocoder: orsGeocoder,
				addWaypoints: false,
				language: 'fr',
				iconLabel: 'Localiser une adresse',
				placeholder: 'Rechercher une adresse…',
			})
			.on('markgeocode', function(e) {
				// Add a waypoint for geocoded position
				if (this.options.addWaypoints) addWaypoint(e.geocode.center);

				// Fit Map to the geocoded bounds
				map.fitBounds(e.geocode.bbox);
			});
		controlGeocoder.addTo(map);

		// ROUTING
		const osrRouter = L.routing.openroute(ors_token, {
			format: 'json',
			language: 'fr',
			"timeout": 10 * 1000, // 10"
			"retries": 2,
			"retryDelay": 1000,
			"retryOn": [],
			routingQueryParams: {
				"attributes": [
					"avgspeed",
					"percentage"
				],
				"elevation": "true",
				"extra_info": [
					"waytype",
					"surface",
					"waycategory",
					"suitability",
					"steepness",
					"tollways",
					"traildifficulty",
					"osmid",
					"roadaccessrestrictions",
					"countryinfo"
				],
				"id": "rounting_request",
				"instructions": "true",
				"instructions_format": "text",
				"language": "fr",
				"maneuvers": "true",
				"options": {
					"avoid_features": [
						"ferries",
						"steps",
						"fords"
					],
					"profile_params": {
						"weightings": {
							"steepness_difficulty": 3
						}
					}
				},
				"continue_straight": "true",
				"geometry": "true",
			}
		});

		// Custom Plan, for markers
		const customPlan = L.Routing.Plan.extend({
				createGeocoders: function() {
					let container = L.Routing.Plan.prototype.createGeocoders.call(this),
						eraseButton = this._eraseWpBtn = createButton('', 'Réinitialiser la route', 'leaflet-routing-erase-waypoint', null, this._buttonContainer),
						exportButton = this._exportRteBtn = createButton('', 'Télécharger la route au format GPX', 'leaflet-routing-export-waypoint hide', 'download_route', this._buttonContainer),
						selectProfil = L.DomUtil.create('select', 'select_profil', this._buttonContainer),
						road = L.DomUtil.create('option', '', selectProfil),
						regular = L.DomUtil.create('option', '', selectProfil),
						mountain = L.DomUtil.create('option', '', selectProfil),
						electric = L.DomUtil.create('option', '', selectProfil);

					road.setAttribute('value', 'cycling-road');
					road.setAttribute('selected', true);
					road.innerHTML = 'Vélo de route / course';
					regular.setAttribute('value', 'cycling-regular');
					regular.innerHTML = 'Vélo de ville / classique';
					mountain.setAttribute('value', 'cycling-mountain');
					mountain.innerHTML = 'VTT / Gravel';
					electric.setAttribute('value', 'cycling-electric');
					electric.innerHTML = 'Vélo électrique (VAE)';

					L.DomEvent.on(selectProfil, 'change', _ => {
						osrRouter.options.profile = selectProfil.value;
						if (plan.isReady()) {
							controlRouting.route();
						}
					});

					L.DomEvent.on(eraseButton, 'click', _ => {
						this.setWaypoints([]);
						if (exportButton && !L.DomUtil.hasClass(exportButton, 'hide')) L.DomUtil.addClass(exportButton, 'hide');
					}, this);
					L.DomEvent.on(exportButton, 'click', _ => {
						// we use Openroute Service wich return the route in GPX format
						let wps = this.getWaypoints();
						if (this.isReady()) {
							let opts = L.extend({}, osrRouter.options, { 'format': 'gpx' });
							const routeurGPX = L.routing.openroute(ors_token, opts);
							routeurGPX.route(wps, gpx => {
								let isodate = (new Date()).toISOString(),
									filename = 'route.' + isodate + '.gpx';

								downloadGPX(gpx, filename);
							});
						}
					}, this);
					return container;
				},
			}),
			plan = new customPlan([], {
				geocoder: orsGeocoder,
				routeWhileDragging: true,
				reverseWaypoints: true,
				language: 'fr',
				dragStyles: [
					{ color: 'black', opacity: 0.15, weight: 9 },
					{ color: 'white', opacity: 0.8, weight: 6 },
					{ color: 'rgb(173, 53, 37)', opacity: 1, weight: 2, dashArray: '7,12' }
				],
				createMarker: function(i, wp, j) {
					let wps = plan.getWaypoints(),
						last = wps.length - 1,
						className = j > wps.length ? '' : i === 0 ? ' marker-start' : i === last ? ' marker-end' : '',
						divIcon = L.divIcon({
							className: 'marker-default' + className,
							iconSize: className ? [20, 20] : [14, 14],
							iconAnchor: className ? [10, 10] : [7, 7],
							html: className ? className === ' marker-start' ? 'A' : 'B' : i,
						}),
						options = {
							draggable: this.draggableWaypoints,
							icon: divIcon,
						},
						marker = L.marker(wp.latLng, options)
						.on('click', L.DomEvent.stop)
						.on('dblclick', function(e) {
							L.DomEvent.stop(e);
							controlRouting.spliceWaypoints(i, 1);
						});
					return marker;
				},
			}).on('waypointgeocoded', function(e) {
				if (e.waypointIndex === 0) {
					let ll = e.waypoint.latLng;
					map.setView(ll, 15);
				}
			});

		// Control Routing (Leaflet Routing Machine)
		const controlRouting = L.routing.control({
				position: 'topright',
				collapsible: true,
				detached: true,
				itineraryDiv: 'itinerary-pane',
				geocodersDiv: 'geocoders-pane',
				router: osrRouter,
				plan: plan,
				formatter: L.routing.formatter_ors({
					language: 'fr',
					roundingSensitivity: -2,
					steptotext: false,
					unitNames: {
						meters: 'm',
						kilometers: 'km',
						yards: 'yd',
						miles: 'mi',
						hours: 'h',
						minutes: '\'',
						seconds: '\"'
					}
				}),
				lineOptions: {
					styles: [
						{ color: 'black', opacity: 0.8, weight: 8 },
						{ color: 'white', opacity: 1, weight: 6 },
						{ color: 'rgb(173, 53, 37)', opacity: 1, weight: 4 }
					]
				},
				pointMarkerStyle: {
					radius: 6,
					color: 'black',
					fillColor: 'white',
					opacity: 1,
					fillOpacity: 1,
					weight: 1,
					pane: 'markerPane',
				},
				waypointMode: 'connect', // snap is more expansive with reverse geocode request
				routeDragInterval: 500,
				totalDistanceRoundingSensitivity: -3,
				routeWhileDragging: true,
				autoRoute: true,
				summaryTemplate: '<h3>{distance} – {time}</h3>',
			})
			.on('routeselected', function(e) {
				let layer = controlRouting.getLine(),
					data = L.polyline(e.route.coordinates).toGeoJSON();
				elevationControl.clear();
				elevationControl.addData(data, layer);
				//if (elevationControl._container.style.display === "none") elevationControl.show();
				sidebar.enablePanel('itinerary-pane');
				sidebar.enablePanel('display-elevation');
				sidebar.enablePanel('fitroute');
				sidebar.enablePanel('dltrace');

				let btn = L.DomUtil.get('download_route');
				L.DomUtil.removeClass(btn, 'hide');
			})
			.on('waypointschanged', function(e) {
				if (!plan.isReady()) {
					let btn = L.DomUtil.get('download_route');
					L.DomUtil.addClass(btn, 'hide');

					elevationControl.clear();
					elevationControl.hide();
					sidebar.disablePanel('itinerary-pane');
					sidebar.disablePanel('display-elevation');
					sidebar.disablePanel('fitroute');
					sidebar.disablePanel('dltrace');
				}
			})
			.on('stepover', function(e) {
				controlRouting.getLine().fire('mousemove', { latlng: e.coord });
			})
			.on('stepout', function(e) {
				controlRouting.getLine().fire('mouseout', { latlng: e.coord })
			});

		controlRouting.addTo(map);

		// Elevation
		const elevationControl = L.control.elevation({
			lazyLoadJS: false,
			theme: "scasb-theme",
			detached: false,
			responsive: false,
			elevationDiv: "#elevation-div",
			autohide: false,
			collapsed: false,
			position: "bottomright",
			followMarker: false,
			imperial: false,
			reverseCoords: false,
			summary: 'line',
			margins: {
				top: 20,
				right: 10,
				bottom: 30,
				left: 45
			},
			width: 480,
			height: 160,
			yAxisMin: 0,
			legend: false,
			slope: 'summary',
			marker: 'position-marker',
			markerIcon: L.divIcon({
				className: 'marker-position',
				iconSize: [14, 14],
				iconAnchor: [7, 7]
			}),
		});
		elevationControl.addTo(map).hide();

		// EasyPrint
		L.easyPrint({
			title: 'Exporter la carte',
			position: 'topright',
			//sizeModes: ['A4Portrait', 'A4Landscape'],
			exportOnly: true,
			hideControlContainer: true,
		}); //.addTo(map);

		// Display Routing Error
		L.Routing.errorControl(controlRouting, {
			header: 'La route ne peut être calculée',
			formatMessage: function(error) {
				if (error.status < 0) {
					return `<code>${error.message}</code>`;
				} else {
					return `<strong>Erreur ${error.status}</strong><br/><em>${error.message}</em>`;
				}
			}
		}).addTo(map);

		map.on('easyPrint-start', function() {
			if (map.hasLayer(reliefLayer)) {
				map.removeLayer(reliefLayer);
				map.once('easyPrint-finished', function() {
					map.addLayer(reliefLayer);
				});
			}
		});

		/* 
		 *  ----------------------------------
		 *            Functions
		 *  ----------------------------------
		 */

		/**
		 * Retourne l'url du Layer IGN
		 * 
		 * @param {String} apikey 
		 * @param {String} layer
		 * 
		 * @returns {String} url
		 */
		function layerIGN(layer, apikey) {
			apikey = apikey || 'choisirgeoportail';
			layer = layer || 'GEOGRAPHICALGRIDSYSTEMS.MAPS';
			let url = "http://wxs.ign.fr/" + apikey +
				"/geoportail/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&" +
				"LAYER=" + layer + "&STYLE=normal&TILEMATRIXSET=PM&" +
				"TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&FORMAT=image%2Fjpeg";
			return url;
		};

		/**
		 * Download GPX file
		 * 
		 * @param {String} gpx XML Document
		 * @param {String} filename 
		 */
		function downloadGPX(gpx, filename) {
			let dlink = L.DomUtil.create('a'),
				blob = new Blob([gpx], { type: 'application/gpx+xml' }),
				url = window.URL.createObjectURL(blob);

			dlink.setAttribute('download', filename);
			dlink.setAttribute('href', url);
			L.DomEvent.on(dlink, 'click', () => {
				setTimeout(function() {
					window.URL.revokeObjectURL(dlink.href);
				}, 1500);
			});
			dlink.click();
			L.DomUtil.remove(dlink);
		};

		/**
		 * Set waypoints on locate with Locate Control
		 * or clicking on the map
		 * Alias function for plan.addWaypoint()
		 * 
		 * @param {Object} L.LatLng() latlng 
		 */
		function addWaypoint(latlng) {
			plan.addWaypoint(latlng);
		};

		/**
		 *
		 * @param {String} label
		 * @param {String} title
		 * @param {Object} container
		 *
		 * @return {Object} DOM Element
		 *
		 * @see http://www.liedman.net/leaflet-routing-machine/tutorials/interaction/
		 */
		function createButton(label, title, className, id, container) {
			let btn = L.DomUtil.create('button', className, container);
			btn.setAttribute('type', 'button');
			btn.setAttribute('title', title);
			if (id) btn.setAttribute('id', id);
			if (label) btn.innerHTML = label;
			return btn;
		};

		function setParent(el, parent) {
			parent.appendChild(el);
		};
	});
})(window, L);
// @license-end